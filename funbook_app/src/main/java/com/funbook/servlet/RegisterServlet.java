package com.funbook.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.funbook.dao.User;
import com.funbook.utility.Validation;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at Register Servlet: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public static Boolean checkEmpty(String value) {
		if(value!=null && !value.isEmpty()) {
			return true;
		}else {
			return false;
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String mobileNumber = request.getParameter("mobileNumber");
		String emailId = request.getParameter("email");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String dob = request.getParameter("dob");
		Integer accountType  = Integer.parseInt(request.getParameter("accountType"));
		
		
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		
		if(checkEmpty(firstName) && checkEmpty(lastName) && Validation.checkMobile(mobileNumber) && Validation.checkEmail(emailId) &&
				checkEmpty(userName) && User.checkUserName(userName) && checkEmpty(password) && Validation.checkDob(dob) )
		{						
			User.signUp(firstName, lastName, mobileNumber, emailId, userName, password, dob, accountType);
			out.println("<font color= green> Sign Up Invoked</font>");
			rd.include(request, response);
		}
		else
		{	
			if(!checkEmpty(firstName)) {
				request.setAttribute("message", "Please Insert First Name");
				rd = request.getRequestDispatcher("error.jsp");
			} else if (!checkEmpty(lastName)){
				request.setAttribute("message", "Please Insert Last Name");
				rd = request.getRequestDispatcher("error.jsp");
			} else if (!Validation.checkMobile(mobileNumber)) {
				request.setAttribute("message", "Invalid Mobile Number");
				rd = request.getRequestDispatcher("error.jsp");
			} else if (!Validation.checkEmail(emailId)) {
				request.setAttribute("message", "Invalid Email");
				rd = request.getRequestDispatcher("error.jsp");
			} else if (!checkEmpty(userName)) {
				request.setAttribute("message", "User name Empty");
				rd = request.getRequestDispatcher("error.jsp");
			} else if (!User.checkUserName(userName)) {
				request.setAttribute("message", "UserName Already Exist");
				rd = request.getRequestDispatcher("error.jsp");
			}else if (!checkEmpty(password)) {
				request.setAttribute("message", "Please Enter Password");
				rd = request.getRequestDispatcher("error.jsp");
			}else if (!Validation.checkDob(dob)) {
				request.setAttribute("message", "Invalid Date of Birth");
				rd = request.getRequestDispatcher("error.jsp");
			}
						
			//request.setAttribute("message", "Please check inserted values of all the fields");
			rd = request.getRequestDispatcher("error.jsp");
			
			rd.forward(request, response);
		}
		
	}

}
